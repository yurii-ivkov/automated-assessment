using System;
using System.Collections.Generic;
using AssessmentTests.Pages;
using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium.Chrome;

namespace AssessmentTests.Tests
{
    [Category("Smoke")]
    [TestFixture]
    [Parallelizable(ParallelScope.Children)]
    public class SmokeTest : BaseTest
	{
		private static IEnumerable<object[]> ValidCredentialsTestData()
		{
			yield return new object[] { "standard_user", "secret_sauce" };
			yield return new object[] { "problem_user", "secret_sauce" };
		}

		[Test]
		[TestCaseSource(nameof(ValidCredentialsTestData))]
		public void LoginWithEmptyCredentials(string username, string password)
        {
			var driver = new ChromeDriver();
			var labsPage = new SwagLabsPage(driver);

			driver.Navigate().GoToUrl("https://www.saucedemo.com/");

			string errorMessage;
			try
			{
				errorMessage = labsPage.LoginWithEmptyFields(username, password);
				driver.Close();
				logger.Information($"Driver in LoginWithEmptyPassword closed.");

				logger.Information($"Error element Message: {errorMessage}");
				Assert.IsTrue(errorMessage.Contains("Username is required"));
			}
			catch(Exception ex)
			{
				logger.Error(ex.Message);
				Assert.Fail();
			}
        }

		[Test]
		[TestCaseSource(nameof(ValidCredentialsTestData))]
		public void LoginWithEmptyPassword(string username, string password)
        {
			var driver = new ChromeDriver();
			var labsPage = new SwagLabsPage(driver);

			driver.Navigate().GoToUrl("https://www.saucedemo.com/");
			
            string errorMessage;

			try
			{
				errorMessage = labsPage.LoginWithEmptyPassword(username, password);

				driver.Close();
				logger.Information($"Driver in LoginWithEmptyPassword closed.");

				logger.Information($"Error Message: {errorMessage}");
				Assert.IsTrue(errorMessage.Contains("Password is required"));
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);
				Assert.Fail();
			}
		}

		[Test]
		[TestCaseSource(nameof(ValidCredentialsTestData))]
		public void LoginWithValidCredentials(string username, string password)
        {
			var driver = new ChromeDriver();
			var labsPage = new SwagLabsPage(driver);

			driver.Navigate().GoToUrl("https://www.saucedemo.com/");

			bool isTitleVisible;
			try
			{
				isTitleVisible = labsPage.Login(username, password);
				logger.Information($"Logged in.");

				driver.Close();
				logger.Information($"Driver in LoginWithValidCredentials closed.");

				Assert.IsTrue(isTitleVisible);
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);
				Assert.Fail();
			}
		}
    }
}