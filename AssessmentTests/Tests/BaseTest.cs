﻿using NUnit.Framework;
using Serilog;
using System.Diagnostics;

namespace AssessmentTests.Tests
{
	public class BaseTest
	{
		protected ILogger logger;
		[SetUp]
		public void Setup()
		{
			Trace.Listeners.Add(new ConsoleTraceListener());
			logger = new LoggerConfiguration()
				.WriteTo.Console()
				.CreateLogger();
			logger.Information("Test set up.");
		}

		[OneTimeTearDown]
		public void EndTest()
		{
			logger.Information("Test ended.");
		}
	}
}
