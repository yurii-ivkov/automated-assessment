﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace AssessmentTests.Pages
{
	public class BasePage
	{
		protected IWebDriver driver;
		protected WebDriverWait wait;

		public BasePage(IWebDriver driver) 
		{ 
			this.driver = driver;
			wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
		}

		public bool ClickElement(By by)
		{
			try
			{
				wait.Until(d => d.FindElement(by)).Click(); 
				return true;
			}
			catch (Exception ex)
			{
                Console.WriteLine(ex.Message);
				return false;
            }
		}

		public bool SendKeys(By by, string key)
		{
			try
			{
				wait.Until(d => d.FindElement(by)).SendKeys(key); 
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return false;
			}
		}

		// Here "Ctrl + a + delete" is used because, as some issues on Github state,
		// React based applications may not always clear field with just Clear.
		// State is remembered, so it just rolls back to prev. state.
		public bool ClearKeys(By by)
		{
			try
			{
				ClickElement(by);
				wait.Until(d => d.FindElement(by)).SendKeys(Keys.Control + "a" + Keys.Delete);
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return false;
			}
		}

		public bool IsVisible(By by)
		{
			return wait.Until(d => d.FindElement(by)).Displayed;
		}

		public string GetText(By by)
		{
			return wait.Until(d => d.FindElement(by)).Text;
		}
	}
}
