﻿using OpenQA.Selenium;
using System;

namespace AssessmentTests.Pages
{
	public class SwagLabsPage : BasePage
	{
		public SwagLabsPage(IWebDriver driver) : base(driver) { }

		public bool Login(string username, string password)
		{
			SendKeys(usernameInput, username);
			SendKeys(passwordInput, password);

			ClickElement(loginButton);

			return IsVisible(title);
		}

		public string LoginWithEmptyFields(string username, string password)
		{
			try
			{
				SendKeys(usernameInput, username);
				SendKeys(passwordInput, password);


                ClearKeys(usernameInput); 
				ClearKeys(passwordInput);


				ClickElement(loginButton);
				return GetText(errorMessage);
			}
			catch (Exception ex)
			{
                Console.WriteLine(ex.Message);
				return string.Empty;
            }
		}

		public string LoginWithEmptyPassword(string username, string password)
		{
			try
			{
				SendKeys(usernameInput, username);
				SendKeys(passwordInput, password);

				ClearKeys(passwordInput);

				ClickElement(loginButton);

				return GetText(errorMessage);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return string.Empty;
			}
		}

		private readonly By usernameInput = By.CssSelector("#user-name");
		private readonly By passwordInput = By.CssSelector("#password");
		private readonly By loginButton = By.CssSelector("#login-button");

		private readonly By errorMessage = By.CssSelector(".error-message-container");

		private readonly By title = By.XPath("//div[text()='Swag Labs']");
	}
}
